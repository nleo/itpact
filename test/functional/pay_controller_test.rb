require 'test_helper'

class PayControllerTest < ActionController::TestCase
  test "should get congratulations" do
    get :congratulations
    assert_response :success
  end

  test "should get attention" do
    get :attention
    assert_response :success
  end

end
