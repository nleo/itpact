require 'ipgeobase'

class PagesController < ApplicationController
  def index
    @products = Product.text_search(params[:query])
    @sections = Section.all
  end

  def show
    @product = Product.find(params[:id])
    products = Product.where section_id:@product.section_id
    @products = products.paginate page: params[:page], order: 'created_at asc', per_page: 4
    @cart = current_cart
    @sections = Section.all
    @section = Section.find(@product.section_id)
    @brand = Brand.find_by_id(@product.brand_id)
    revi = Review.where product_id:@product.id
    @review = revi.first
    @images = @product.product_images
    ip = request.remote_ip
    @ip_meta = Ipgeobase.lookup(ip)
    @deliveries = Delivery.where city: @ip_meta.city
    



    respond_to do |format|
      format.html # show2.html.erb
      format.json { render json: @product }
    end
  end

  def comments
    @reviews = Review.scoped
    @review = Review.new
  end

  def edit
    @review = Review.find(params[:id])
  end  
  
  private

  def correct_user
    if user_signed_in?
      @review = current_user.reviews.find_by_id(params[:id])
    end
  end	

end



