class SectionsController < ApplicationController
  #before_filter :authenticate_user!, except: [:show]
  before_filter :authenticate_admin!, except: [:show]
  # GET /sections
  # GET /sections.json
  def index
    @sections = Section.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sections }
    end
  end

  # GET /sections/1
  # GET /sections/1.json
  def show
    @sections = Section.all
    @section = Section.find(params[:id])
    @cart = current_cart
    if params[:novelty] == "true"
      products = Product.where novelty: true 
      #products = @section.products.where novelty: true 
    else
      if params[:popular] == "true"
        products = Product.where popular: true
        #products = @section.products.where popular: true
      else  
       products = @section.products
      end 
    end
    if params[:filter] == "true"
      @filter = true
    else
      @filter = false  
    end  
    @products = products.paginate page: params[:page], order: 'created_at asc', per_page: 8
    ip = request.remote_ip
    @ip_meta = Ipgeobase.lookup(ip)
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @section }
    end
  end

  # GET /sections/new
  # GET /sections/new.json
  def new
    @section = Section.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @section }
    end
  end

  # GET /sections/1/edit
  def edit
    @section = Section.find(params[:id])
  end

  # POST /sections
  # POST /sections.json
  def create
    @section = Section.new(params[:section])

    respond_to do |format|
      if @section.save
        format.html { redirect_to @section, notice: 'Section was successfully created.' }
        format.json { render json: @section, status: :created, location: @section }
      else
        format.html { render action: "new" }
        format.json { render json: @section.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sections/1
  # PUT /sections/1.json
  def update
    @section = Section.find(params[:id])

    respond_to do |format|
      if @section.update_attributes(params[:section])
        format.html { redirect_to @section, notice: 'Section was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @section.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sections/1
  # DELETE /sections/1.json
  def destroy
    @section = Section.find(params[:id])
    @section.destroy

    respond_to do |format|
      format.html { redirect_to sections_url }
      format.json { head :no_content }
    end
  end
end
