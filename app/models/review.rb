class Review < ActiveRecord::Base
  attr_accessible :content, :product_id, :user_id, :ancestry, :parent_id
  has_ancestry
  belongs_to :user
  belongs_to :product
end
