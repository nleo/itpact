class ProductImage < ActiveRecord::Base
  attr_accessible :image, :product_id
  mount_uploader :image, ImageUploader
  belongs_to :product, :polymorphic => true
end
