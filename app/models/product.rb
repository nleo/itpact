class Product < ActiveRecord::Base
  attr_accessible :article, :brand_id, :delivery_method, :description, :function, :image_link, :name, :novelty, :novelty_at, :novelty_change_at, :novelty_force, :novelty_margin, :price, :section_id, :popular, :popular_lock, :product_images_attributes
  has_many :line_items
  has_many :reviews, dependent: :destroy
  has_many :product_images, :dependent => :destroy  
  accepts_nested_attributes_for :product_images, :allow_destroy => true

  def self.text_search(query)
    if query.present?
      where "to_tsvector('russian', name) @@ to_tsquery('russian', :q) or to_tsvector('russian', description) @@ to_tsquery('russian', :q)", q: query     
    else
      scoped
    end
  end 

  before_destroy :ensure_not_referenced_by_any_line_item
  
  private

  #убеждаемся в отсутствии товарных позиций,
  #ссылающихся на данный товар

  

  # ensure that there are no line items referencing this product
    def ensure_not_referenced_by_any_line_item
      if line_items.empty?
        return true
      else
        errors.add(:base, 'Line Items present')
        return false
      end
    end	

end
