class Order < ActiveRecord::Base
  attr_accessible :status, :user_id, :address, :email, :full_name
  has_many :line_items, dependent: :destroy

  after_save :popular_and_novelty_calculation

  def popular_and_novelty_calculation
    var = Var.first
  	products = Product.all
  	products.each do |product|
  	  col = 0	
      line_items = LineItem.where product_id:(product.id)
      if product.novelty == true
      	if product.novelty_at < Date.today.advance(:days => - var.novelty_duration)
      	  product.update_attribute(:novelty, "false")
        end
      end
      if product.popular_lock == false  	
        line_items.where("created_at > ?", Time.zone.now.advance(:days => - var.novelty_period)).each do |line_item|
          col = col + line_item.quantity 
        end
        if col > product.popular_sale
          product.update_attribute(:popular, true)
        end
      end  
    end
  end




  def add_line_items_from_cart(cart)
    cart.line_items.each do |item|
      item.cart_id = nil
      line_items << item
    end
  end  
end
