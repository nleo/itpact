class Section < ActiveRecord::Base
  attr_accessible :name, :parent_id

  has_many :products
  belongs_to :product

end
