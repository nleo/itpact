class CreateVars < ActiveRecord::Migration
  def change
    create_table :vars do |t|
      t.integer :novelty_period
      t.integer :novelty_duration

      t.timestamps
    end
  end
end
