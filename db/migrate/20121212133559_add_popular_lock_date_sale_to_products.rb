class AddPopularLockDateSaleToProducts < ActiveRecord::Migration
  def change
    add_column :products, :popular_lock, :boolean, :null => false, :default => "false"
    add_column :products, :popular_date, :datetime
    add_column :products, :popular_sale, :integer, :default => 10
  end
end
