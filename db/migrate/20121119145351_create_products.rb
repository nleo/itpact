class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :article
      t.string :name
      t.text :description
      t.decimal :price
      t.boolean :novelty
      t.date :novelty_at
      t.decimal :novelty_margin
      t.date :novelty_change_at
      t.boolean :novelty_force
      t.string :delivery_method
      t.string :function
      t.integer :brand_id
      t.string :image_link

      t.timestamps
    end
  end
end
