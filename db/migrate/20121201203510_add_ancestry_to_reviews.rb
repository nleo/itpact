class AddAncestryToReviews < ActiveRecord::Migration
  def change
    add_column :reviews, :ancestry, :string
  end
end
