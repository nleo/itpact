class CreateDeliveries < ActiveRecord::Migration
  def change
    create_table :deliveries do |t|
      t.string :city
      t.decimal :price
      t.string :how

      t.timestamps
    end
  end
end
