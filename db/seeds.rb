#encoding: utf-8

# coding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)




35.times do |n|
Product.create!(:name => "Антивирус #{n}",
                :section_id => 1,
                :article => "A#{n}",
                :description => "Kaspersky Internet Security - защита от интернет-угроз в режиме реального времени.

Ключевые функции:
Безопасные платежи
Гибридная защита
Безопасность личных данных
Блокирование опасных программ и веб-сайтов
Безопасное общение в социальных сетях
Родительский контроль
Дополнительная защита от эксплойтов",
                :price => 16.00,
                :delivery_method => "download + email",
                :function => "Защита от интернет-угроз в режиме реального времени.",                
                :brand_id => 3,
                :image_link => "1.png")
end
35.times do |n|
Product.create!(:name => "Операционная система #{n}",
                :section_id => 2,
                :article => "B#{n}",
                :description => "Kaspersky Internet Security - защита от интернет-угроз в режиме реального времени.

Ключевые функции:
Безопасные платежи
Гибридная защита
Безопасность личных данных
Блокирование опасных программ и веб-сайтов
Безопасное общение в социальных сетях
Родительский контроль
Дополнительная защита от эксплойтов",
                :price => 1.00,
                :delivery_method => "download + email",
                :function => "Защита от интернет-угроз в режиме реального времени.",                
                :brand_id => 3,
                :image_link => "1.png")
end
35.times do |n|
Product.create!(:name => "Офисная программа #{n}",
                :section_id => 3,
                :article => "С#{n}",
                :description => "Kaspersky Internet Security - защита от интернет-угроз в режиме реального времени.

Ключевые функции:
Безопасные платежи
Гибридная защита
Безопасность личных данных
Блокирование опасных программ и веб-сайтов
Безопасное общение в социальных сетях
Родительский контроль
Дополнительная защита от эксплойтов",
                :price => 10.00,
                :delivery_method => "download + email",
                :function => "Защита от интернет-угроз в режиме реального времени.",                
                :brand_id => 3,
                :image_link => "1.png")
end
35.times do |n|
Product.create!(:name => "Бухгалтерия #{n}",
                :section_id => 4,
                :article => "D#{n}",
                :description => "Kaspersky Internet Security - защита от интернет-угроз в режиме реального времени.

Ключевые функции:
Безопасные платежи
Гибридная защита
Безопасность личных данных
Блокирование опасных программ и веб-сайтов
Безопасное общение в социальных сетях
Родительский контроль
Дополнительная защита от эксплойтов",
                :price => 1.00,
                :delivery_method => "download + email",
                :function => "Защита от интернет-угроз в режиме реального времени.",                
                :brand_id => 3,
                :image_link => "1.png")
end                    
35.times do |n|
Product.create!(:name => "САПР #{n}",
                :section_id => 5,
                :article => "E#{n}",
                :description => "Kaspersky Internet Security - защита от интернет-угроз в режиме реального времени.

Ключевые функции:
Безопасные платежи
Гибридная защита
Безопасность личных данных
Блокирование опасных программ и веб-сайтов
Безопасное общение в социальных сетях
Родительский контроль
Дополнительная защита от эксплойтов",
                :price => 10.00,
                :delivery_method => "download + email",
                :function => "Защита от интернет-угроз в режиме реального времени.",                
                :brand_id => 3,
                :image_link => "1.png")
end                    
35.times do |n|
Product.create!(:name => "Управление бизнесом #{n}",
                :section_id => 6,
                :article => "E#{n}",
                :description => "Kaspersky Internet Security - защита от интернет-угроз в режиме реального времени.

Ключевые функции:
Безопасные платежи
Гибридная защита
Безопасность личных данных
Блокирование опасных программ и веб-сайтов
Безопасное общение в социальных сетях
Родительский контроль
Дополнительная защита от эксплойтов",
                :price => 10.00,
                :delivery_method => "download + email",
                :function => "Защита от интернет-угроз в режиме реального времени.",                
                :brand_id => 3,
                :image_link => "1.png")
end                    
35.times do |n|
Product.create!(:name => "Графика и дизайн #{n}",
                :section_id => 7,
                :article => "F#{n}",
                :description => "Kaspersky Internet Security - защита от интернет-угроз в режиме реального времени.

Ключевые функции:
Безопасные платежи
Гибридная защита
Безопасность личных данных
Блокирование опасных программ и веб-сайтов
Безопасное общение в социальных сетях
Родительский контроль
Дополнительная защита от эксплойтов",
                :price => 10.00,
                :delivery_method => "download + email",
                :function => "Защита от интернет-угроз в режиме реального времени.",                
                :brand_id => 3,
                :image_link => "1.png")
end                    
35.times do |n|
Product.create!(:name => "Мультимедиа #{n}",
                :section_id => 8,
                :article => "G#{n}",
                :description => "Kaspersky Internet Security - защита от интернет-угроз в режиме реального времени.

Ключевые функции:
Безопасные платежи
Гибридная защита
Безопасность личных данных
Блокирование опасных программ и веб-сайтов
Безопасное общение в социальных сетях
Родительский контроль
Дополнительная защита от эксплойтов",
                :price => 6.00,
                :delivery_method => "download + email",
                :function => "Защита от интернет-угроз в режиме реального времени.",                
                :brand_id => 3,
                :image_link => "1.png")
end
35.times do |n|
Product.create!(:name => "Файлы и диски #{n}",
                :section_id => 9,
                :article => "1000#{n}",
                :description => "Kaspersky Internet Security - защита от интернет-угроз в режиме реального времени.

Ключевые функции:
Безопасные платежи
Гибридная защита
Безопасность личных данных
Блокирование опасных программ и веб-сайтов
Безопасное общение в социальных сетях
Родительский контроль
Дополнительная защита от эксплойтов",
                :price => 60.00,
                :delivery_method => "download + email",
                :function => "Защита от интернет-угроз в режиме реального времени.",                
                :brand_id => 3,
                :image_link => "1.png")
end
35.times do |n|
Product.create!(:name => "Системные программы #{n}",
                :section_id => 10,
                :article => "1000#{n}",
                :description => "Kaspersky Internet Security - защита от интернет-угроз в режиме реального времени.

Ключевые функции:
Безопасные платежи
Гибридная защита
Безопасность личных данных
Блокирование опасных программ и веб-сайтов
Безопасное общение в социальных сетях
Родительский контроль
Дополнительная защита от эксплойтов",
                :price => 60.00,
                :delivery_method => "download + email",
                :function => "Защита от интернет-угроз в режиме реального времени.",                
                :brand_id => 3,
                :image_link => "1.png")
end
35.times do |n|
Product.create!(:name => "Сеть и Интернет #{n}",
                :section_id => 11,
                :article => "1000#{n}",
                :description => "Kaspersky Internet Security - защита от интернет-угроз в режиме реального времени.

Ключевые функции:
Безопасные платежи
Гибридная защита
Безопасность личных данных
Блокирование опасных программ и веб-сайтов
Безопасное общение в социальных сетях
Родительский контроль
Дополнительная защита от эксплойтов",
                :price => 60.00,
                :delivery_method => "download + email",
                :function => "Защита от интернет-угроз в режиме реального времени.",                
                :brand_id => 3,
                :image_link => "1.png")
end
35.times do |n|
Product.create!(:name => "Программирование #{n}",
                :section_id => 12,
                :article => "1000#{n}",
                :description => "Kaspersky Internet Security - защита от интернет-угроз в режиме реального времени.

Ключевые функции:
Безопасные платежи
Гибридная защита
Безопасность личных данных
Блокирование опасных программ и веб-сайтов
Безопасное общение в социальных сетях
Родительский контроль
Дополнительная защита от эксплойтов",
                :price => 60.00,
                :delivery_method => "download + email",
                :function => "Защита от интернет-угроз в режиме реального времени.",                
                :brand_id => 3,
                :image_link => "1.png")
end
35.times do |n|
Product.create!(:name => "Другое #{n}",
                :section_id => 13,
                :article => "1000#{n}",
                :description => "Kaspersky Internet Security - защита от интернет-угроз в режиме реального времени.

Ключевые функции:
Безопасные платежи
Гибридная защита
Безопасность личных данных
Блокирование опасных программ и веб-сайтов
Безопасное общение в социальных сетях
Родительский контроль
Дополнительная защита от эксплойтов",
                :price => 16.00,
                :delivery_method => "download + email",
                :function => "Защита от интернет-угроз в режиме реального времени.",                
                :brand_id => 3,
                :image_link => "1.png")
end                    
Section.create!(:name => "Aнтивирусы")
Section.create!(:name => "Операционные системы")
Section.create!(:name => "Офисные программы")
Section.create!(:name => "Бухгалтерия")
Section.create!(:name => "САПР")
Section.create!(:name => "Управление бизнесом")
Section.create!(:name => "Графика и дизайн")
Section.create!(:name => "Мультимедиа")
Section.create!(:name => "Файлы и диски")
Section.create!(:name => "Системные программы")
Section.create!(:name => "Сеть и Интернет")
Section.create!(:name => "Программирование")
Section.create!(:name => "Другое")

Brand.create!(:name => "Microsoft")
Brand.create!(:name => "Sun")
Brand.create!(:name => "Каspersky Lab")

Var.create!(:novelty_duration => 60, :novelty_period => 60)